# Blank 4 README #

Hello, I am Blank 4, the lightweight PHP framework for usage in small and medium-size projects! I try to help you build easily manageable projects. 
I like you all and I don't want to put you the minecraft wooden blocks under your feet. You can run me by simply copying all my content and by keeping some setup done you can create even greater things than I am!

### Great, but how can I start to use you? ###

When you want to start some project with me, you need only my source code. Basicly I have some things already preset so first launch is a matter of minutes. 

1. download the source,
2. create the vhost for my project,
3. run the application

### How do you work, man? ###

I work as any other classic MVC-pattern-based framework. I use various types of files to manage specific parts of website. 
There are view files that cover HTML part of pages. Then there are models that you set to cooperate with database data. And lastly, there are controllers that connect both of those things.

Just to highlight the most important controller of all - there is a controller named **Gear**. This controller is responsible for all actions that seem to happen on every speciffic site you visit in your project. 

### How can I change you? ###

Everything what is most likely to be changeable, is located in two my files - settings-local.json and settings-production.json. I hope I don't have to describe you meaning of those files. 

* In every file you can setup things like version of your app, author, various fixed metadata for website. 
* Then you can specify your custom-made routes. You simply set what kind of address leads to which Gear controller. This is some kind of .htaccess setup, but more simple. 
* As third part of setup there are fixed redirections. Those redirections specify the locations you will be redirected to, when they are called from their controllers such as built-in Sign controller.
* You can also setup database connections here. I hope I don't have to write you the basics of the DB connecting. I just mention that a SQLite connection is preset here. 
* Finally, you can setup here a translations. They can be opened and closed freely and you can specify predefined translation to be used when first visit of your page. This feature can be updated freely.

### Great! What now? ###

Sadly, a full manual is not ready yet, however a basic code and built-in files are quite heavy documented. If you're looking for any additional "first-step" manuals, you will have to wait. However, even Minecraft doesn't have any sort of manuals included yet milions of people play it. Why you should not play with me the same way?

### Hum, okay. Who made you? ###

My father is Marek J. Kolcun. He used me to build his own [representional website][1] and is free to accept any critique or words of wisdom that will help him to make me better next time.

[1]: http://jocho.sk