<?php
namespace Blank;
class Pager extends Gear {
	private $page;
	private $totalPages;
	private $itemsOnPage;

	public function init() {
		$this->page = (isset($this->router->query['page'])) ? intval($this->router->query['page']) : 1;
		if (!$this->session->has('page') || $this->session->page != $this->page)
			$this->session->set('page', $this->page);
		$this->session->save();
		$this->itemsOnPage = 10;
	}

	public function setup($page, $totalItems, $itemsOnPage = 10) {
		$page = $this->page;
		$this->itemsOnPage = ($itemsOnPage > 0) ? $itemsOnPage : $this->itemsOnPage;
		$shownPages = array();

		$totalPages = ceil($totalItems / $this->itemsOnPage);

		if ($page < 1)
			$page = 1;
		if ($page > $totalPages)
			$page = $totalPages;

		$pagesAroundPage = array_unique(array(1, 2, $page-1, $page, $page+1, $totalPages-1, $totalPages));
		foreach ($pagesAroundPage as $key => $pap)
			if ($pap < 1 || $pap > $totalPages)
				unset($pagesAroundPage[$key]);
		$pagesAroundPage = array_values($pagesAroundPage);

		$counter = count ($pagesAroundPage);
		$hellIpedArray = array();
		for ($i = 0; $i < $counter; $i++) {
			$hellIpedArray[] = $pagesAroundPage[$i];
			if ($i+1 < $counter && $pagesAroundPage[$i]+1 < $pagesAroundPage[$i+1])
				$hellIpedArray[] = '&hellip;';
		}
		$shownPages = $hellIpedArray;

		$this->template->shownPages = $shownPages;
		$this->template->page = $page;
		$this->template->totalPages = $totalPages;

		return array($shownPages, $page, $totalPages);
		
	}

	public function getItemsOnPage() {
		return $this->itemsOnPage;
	}

	public function render() {}
}
?>